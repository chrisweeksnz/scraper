FROM alpine:latest

RUN apk update && \
  apk --no-cache add \
    bash \
    ca-certificates \
    curl \
    git \
    jq \
    python3 \
    yq-python

CMD ["bash"]
